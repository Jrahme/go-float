package tmux

import (
	"log"
	"os/exec"
)

type ITestExecutor interface {
	runTestCmd(...string) ([]string, *exec.Cmd, error)
	printError(cmd *exec.Cmd, output []string, err error)
}

type TestExecutor struct {
	IExecute
	Socket string
}

func (t TestExecutor) runTestCmd(args ...string) ([]string, *exec.Cmd, error) {
	args = append([]string{"-S", t.Socket}, args...)
	cmd := exec.Command(tmuxBinary, args...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return splitByNewLine(string(out)), cmd, err
	}
	return splitByNewLine(string(out)), cmd, nil
}

func (t TestExecutor) printError(cmd *exec.Cmd, output []string, err error) {
	log.Println("-----------------------")
	log.Println("Error executing command")
	log.Printf("%#v\n", output)
	log.Printf("%#v\n", cmd)
	log.Printf("%#v\n", err)
	log.Println("-----------------------")
}
