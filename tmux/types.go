package tmux

type State interface {
	CurrentState() (map[string]Session, error)
	FloatExists(string) (bool, error)
}

type Pane struct {
	Name string
}
type Window struct {
	Index string
	Name  string
	Panes map[string]Pane
}
type Session struct {
	Name    string
	Windows map[string]Window
}
