package tmux

import (
	"fmt"
	"github.com/google/uuid"
	"gotest.tools/v3/assert"
	"log"
	"os"
	"os/exec"
	"strings"
	"testing"
	"time"
)

func closeTmux(socket string) {
	killCmd := exec.Command("tmux", "-S", socket, "kill-server")
	err := killCmd.Run()
	if err != nil {
		log.Fatal(fmt.Sprintf("Could not stop server at socket %s, please kill server manually.", socket))
	}
	err = os.Remove(socket)
	if err != nil {
		log.Fatal(fmt.Sprintf("Could not remove tmux socket file at %s", socket))
	}
}

func newTmuxServer() (string, error) {
	socketUUID, err := uuid.NewRandom()
	socket := socketUUID.String()

	if err != nil {
		return "", err
	}
	socketPath := fmt.Sprintf("/tmp/tmux-1000/%s", socket)
	_, err = os.Stat(socketPath)
	if err != nil {
		if os.IsNotExist(err) {
			f, err := os.Create(socketPath)
			if err != nil {
				panic(err)
			}
			defer f.Close()
		} else {
			return "", err
		}
	}

	newTmuxCmd := exec.Command("tmux", "-L", socket, "new-session", "-d")
	err = newTmuxCmd.Start()
	if err != nil {
		return "", err
	}
	time.Sleep(time.Second * 1)
	return socketPath, nil
}

func initTestingTmux() (Tmux, error) {
	//initialize a new tmux server
	serverSocket, err := newTmuxServer()
	if err != nil {
		return Tmux{}, err
	}

	executor := TestExecutor{
		NewExecutor(serverSocket),
		serverSocket,
	}
	modifier := TestModifier{
		NewModifier(executor),
		executor,
	}

	inspector := TestInspector{
		NewInspector(executor),
		executor,
	}
	tmux := Tmux{
		inspector,
		modifier,
		executor,
	}

	return tmux, nil
}

func Test_TestingInterfaces(t *testing.T) {
	tmux, err := initTestingTmux()
	socket := tmux.Executor.(TestExecutor).Socket
	defer closeTmux(socket)
	assert.NilError(t, err)

	// confirm assumptions about initial tmux state
	windows, err := tmux.Inspector.(ITestInspectors).WindowCount("0")
	assert.NilError(t, err)
	assert.Equal(t, windows, 1)

	panes, err := tmux.Inspector.(ITestInspectors).PaneCount("0:1")
	assert.NilError(t, err)
	assert.Equal(t, panes, 1)

	// test modifications and observing them
	err = tmux.Modifier.(ITestModifiers).WriteToPane("0:1.1", "This is a test")
	assert.NilError(t, err)
	lines, err := tmux.Inspector.(ITestInspectors).ReadFromPane("0:1.1")
	assert.NilError(t, err)
	assert.Check(t, linesContainValue(lines, "This is a test"))

	err = tmux.Modifier.(ITestModifiers).SplitPane("0:1.1", false)
	assert.NilError(t, err)

	//check for how many panes exist
	panes, err = tmux.Inspector.(ITestInspectors).PaneCount("0:1")
	assert.NilError(t, err)
	assert.Equal(t, panes, 2)

	err = tmux.Modifier.(ITestModifiers).CreateWindow("0")
	assert.NilError(t, err)

	windows, err = tmux.Inspector.(ITestInspectors).WindowCount("0")
	assert.NilError(t, err)
	assert.Equal(t, windows, 2)

	err = tmux.Modifier.(ITestModifiers).DestroyWindow("0:2")
	assert.NilError(t, err)

	windows, err = tmux.Inspector.(ITestInspectors).WindowCount("0")
	assert.NilError(t, err)
	assert.Equal(t, windows, 1)

}

func linesContainValue(l []string, v string) bool {
	for _, s := range l {
		if strings.Contains(v, s) {
			return true
		}
	}
	return false
}
