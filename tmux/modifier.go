package tmux

import "fmt"

type IModify interface {
	FloatPane(string) error
	AttachPane(string) error
	CreateFloatingSession() error
}

type Modifier struct {
	executor IExecute
}

func (m Modifier) FloatPane(pane string) error {
	_, err := m.executor.RunCmd("break-pane", "-n", pane, "-t", "floating")
	return err
}

func (m Modifier) AttachPane(float string) error {
	windowName := fmt.Sprintf("%s:%s", FloatingSession, float)
	_, err := m.executor.RunCmd("join-pane", "-hs", windowName)
	return err
}

func (m Modifier) CreateFloatingSession() error {
	_, err := m.executor.RunCmd("new-session", "-t", "floating", "-s", "floating", "-d")
	return err
}

func NewModifier(e IExecute) Modifier {
	return Modifier{executor: e}
}
