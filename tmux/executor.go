package tmux

import (
	"os/exec"
	"strings"
)

const tmuxBinary = "tmux"

type IExecute interface {
	RunCmd(...string) ([]string, error)
}

type Executor struct {
	socket string
}

func NewExecutor(socket string) Executor {
	return Executor{socket}
}

func (e Executor) RunCmd(args ...string) ([]string, error) {
	args = append([]string{"-S", e.socket}, args...)
	cmd := exec.Command(tmuxBinary, args...)
	out, err := cmd.Output()
	if err != nil {
		return []string{}, err
	}
	return splitByNewLine(string(out)), nil

}

// TODO this belongs somewhere else since I also use it in tests
// it needs it's own unit test too
func splitByNewLine(s string) []string {
	split := strings.Split(s, "\n")
	if len(split) > 1 {
		return split[:len(split)-1]
	}
	return split
}
