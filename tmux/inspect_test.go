package tmux

type ITestInspectors interface {
	ReadFromPane(string) ([]string, error)
	PaneCount(string) (int, error)
	WindowCount(session string) (int, error)
}

type TestInspector struct {
	IInspect
	ITestExecutor
}

func (t TestInspector) ReadFromPane(target string) ([]string, error) {
	result, cmd, err := t.ITestExecutor.runTestCmd("capture-pane", "-t", target)
	if err != nil {
		t.ITestExecutor.printError(cmd, result, err)
		return []string{}, err
	}
	return result, nil
}

func (t TestInspector) PaneCount(target string) (int, error) {
	result, cmd, err := t.ITestExecutor.runTestCmd("list-panes", "-t", target)
	if err != nil {
		t.ITestExecutor.printError(cmd, result, err)
		return 0, err
	}

	return len(result), nil
}

func (t TestInspector) WindowCount(session string) (int, error) {
	result, cmd, err := t.ITestExecutor.runTestCmd("list-windows", "-t", session)
	if err != nil {
		t.ITestExecutor.printError(cmd, result, err)
	}
	return len(result), err
}
