package tmux

const FloatingSession = "floating"

type Tmux struct {
	Inspector IInspect
	Modifier  IModify
	Executor  IExecute
}

func NewLocalTmux() (Tmux, error) {
	return NewTmux("/tmp/tmux-1000/default")
}

func NewTmux(socket string) (Tmux, error) {
	exec := NewExecutor(socket)
	var inspect IInspect = NewInspector(exec)
	var modify IModify = NewModifier(exec)
	t := Tmux{
		Modifier:  modify,
		Inspector: inspect,
		Executor:  exec,
	}
	sessionExists, err := t.Inspector.SessionExists(FloatingSession)

	if err != nil {
		return Tmux{}, err
	}
	if !sessionExists {
		err := t.Modifier.CreateFloatingSession()
		if err != nil {
			return Tmux{}, err
		}
	}

	return t, nil
}

func (t Tmux) AttachPane(floatName string) error {
	err := t.Modifier.AttachPane(floatName)
	return err
}

func (t Tmux) FloatPane(floatName string) error {
	t.Modifier.FloatPane(floatName)
	return nil
}

//func (t *Tmux) TmuxState() (map[string]Session, error){
//	state, err := t.mapSessions()
//	if err != nil {
//		return nil, err
//	}
//	return state, nil
//}
//
//func (t *Tmux) FloatExists(floatName string) (bool,error){
//	windows, err := t.listWindows("floating")
//	if err != nil {
//		return true, err
//	}
//	for _, w := range windows{
//		if w == floatName{
//			return true, nil
//		}
//	}
//	return false, nil
//}
//
//
//func (t *Tmux) listPanes(session, window string) ([]string, error){
//	windowID := fmt.Sprintf("%s:%s", session, window)
//	res, err := t.executor.RunCmd("tmux", "list-panes", "-t", windowID, "-F", "#{pane_index}")
//	if err != nil{
//		return nil, err
//	}
//	return res, nil
//}
//
//// stuff for modelling a tmux server
//func (t *Tmux) mapSessions() (map[string]Session, error){
//	sessionMap := make(map[string]Session)
//	sessions, err := t.listSessionsByName()
//	if err != nil{
//		return sessionMap, err
//	}
//	for _, s := range sessions{
//		windows, err := t.mapWindows(s)
//		if err != nil {
//			return sessionMap, err
//		}
//		sessionMap[s] = Session{
//			Name: s,
//			Windows: windows,
//		}
//	}
//	return sessionMap, nil
//}
//
//func (t *Tmux) mapWindows(session string) (map[string]Window, error){
//	winMap := make(map[string]Window)
//	windows, err := t.listWindows(session)
//	if err != nil{
//		return winMap, err
//	}
//	for _, w := range windows{
//		vals := strings.Split(w, "::")
//		panes, err := t.mapPanes(session, vals[0])
//		if err != nil {
//			return winMap, err
//		}
//		win := Window{
//			Index: vals[0],
//			Name: vals[1],
//			Panes: panes,
//		}
//		winMap[vals[0]] = win
//	}
//	return winMap, err
//}
//
//
//func (t *Tmux) mapPanes(session, window string) (map[string]Pane, error){
//	paneMap :=  make(map[string]Pane)
//	panes, err := t.listPanes(session, window)
//	if err != nil{
//		return paneMap, err
//	}
//	for _, p := range panes{
//		paneMap[p] = Pane{Name: p}
//	}
//	return paneMap, nil
//}
//
