package tmux

import (
	"fmt"
	"strings"
)

type IInspect interface {
	SessionExists(string) (bool, error)
	WindowExists(string, string) (bool, error)
	CurrentSession() (string, error)
	MapTmuxSession(string) (Session, error)
	listPanes(session, window string) ([]string, error)
	mapPanes(session, window string) (map[string]Pane, error)
	mapWindows(session string) (map[string]Window, error)
	listWindows(session string) ([]string, error)
	listSessionsByName() ([]string, error)
}

type Inspect struct {
	executor IExecute
}

func NewInspector(e IExecute) Inspect {
	return Inspect{e}
}

func (i Inspect) SessionExists(sessionName string) (bool, error) {
	sessions, err := i.listSessionsByName()
	if err != nil {
		return false, err
	}
	for _, s := range sessions {
		if s == sessionName {
			return true, nil
		}
	}
	return false, nil
}

func (i Inspect) WindowExists(session, window string) (bool, error) {
	windowName := formatWindowName(session, window)
	windows, err := i.listWindows(session)
	if err != nil {
		return false, err
	}
	for _, w := range windows {
		if w == windowName {
			return true, nil
		}
	}
	return false, nil
}

func (i Inspect) CurrentSession() (string, error) {
	r, err := i.executor.RunCmd("display-message", "-p", "#S")
	if err != nil {
		return "", err
	}
	return r[0], err
}

func (i Inspect) listSessionsByName() ([]string, error) {
	res, err := i.executor.RunCmd("list-sessions", "-F", "#{session_name}")
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (i Inspect) listWindows(session string) ([]string, error) {
	res, err := i.executor.RunCmd("list-windows", "-t", session, "-F", "#{window_index}::#{window_name}")
	if err != nil {
		return nil, err
	}
	return res, nil
}

// for testing
func (i Inspect) MapTmuxSession(name string) (Session, error) {
	exists, err := i.SessionExists(name)
	if err != nil {
		return Session{}, err
	}
	if !exists {
		return Session{}, nil
	}
	windows, err := i.mapWindows(name)
	if err != nil {
		return Session{}, nil
	}
	session := Session{
		Name:    name,
		Windows: windows,
	}
	return session, nil
}

func (i Inspect) mapWindows(session string) (map[string]Window, error) {
	winMap := make(map[string]Window)
	windows, err := i.listWindows(session)
	if err != nil {
		return winMap, err
	}
	for _, w := range windows {
		vals := strings.Split(w, "::")
		panes, err := i.mapPanes(session, vals[0])
		if err != nil {
			return winMap, err
		}
		win := Window{
			Index: vals[0],
			Name:  vals[1],
			Panes: panes,
		}
		winMap[vals[0]] = win
	}
	return winMap, err
}

func (i Inspect) mapPanes(session, window string) (map[string]Pane, error) {
	paneMap := make(map[string]Pane)
	panes, err := i.listPanes(session, window)
	if err != nil {
		return paneMap, err
	}
	for _, p := range panes {
		paneMap[p] = Pane{Name: p}
	}
	return paneMap, nil
}

func (i Inspect) listPanes(session, window string) ([]string, error) {
	windowID := fmt.Sprintf("%s:%s", session, window)
	res, err := i.executor.RunCmd("tmux", "list-panes", "-t", windowID, "-F", "#{pane_index}")
	if err != nil {
		return nil, err
	}
	return res, nil
}

func formatWindowName(session, window string) string {
	return fmt.Sprintf("%s::%s", session, window)
}
