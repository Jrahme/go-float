package tmux

type ITestModifiers interface {
	SplitPane(string, bool) error
	CreateWindow(string) error
	DestroyWindow(string) error
	WriteToPane(string, string) error
}

type TestModifier struct {
	IModify
	ITestExecutor
}

func (t TestModifier) SplitPane(target string, horizontal bool) error {
	cmds := []string{"-t", "target"}
	if horizontal {
		cmds = append(cmds, "-h")
	}
	cmds = append(cmds, []string{"split-pane"}...)

	out, cmd, err := t.ITestExecutor.runTestCmd("split-pane", "-t", target)
	if err != nil {
		t.ITestExecutor.printError(cmd, out, err)
	}
	return err
}

func (t TestModifier) WriteToPane(target, content string) error {
	out, cmd, err := t.ITestExecutor.runTestCmd("send-keys", "-t", target, content)

	if err != nil {
		t.ITestExecutor.printError(cmd, out, err)
	}
	return err
}

func (t TestModifier) CreateWindow(session string) error {
	out, cmd, err := t.ITestExecutor.runTestCmd("new-window", "-t", session)
	if err != nil {
		t.ITestExecutor.printError(cmd, out, err)
	}
	return err
}

func (t TestModifier) DestroyWindow(target string) error {
	out, cmd, err := t.ITestExecutor.runTestCmd("kill-window", "-t", target)
	if err != nil {
		t.ITestExecutor.printError(cmd, out, err)
	}
	return err
}

type TestModifierExecutor struct {
	IExecute
}

func (t TestModifierExecutor) RunCmd(args ...string) ([]string, error) {
	return []string{""}, nil
}
