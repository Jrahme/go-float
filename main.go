package main

import (
	"fmt"
	"os/exec"
)

func main() {
	//	cmd.Execute()
	c := exec.Command("tmux", "-S", "/tmp/tmux-1000/de653386-5001-4d01-b09b-f5a314d0deb0", "list-panes")
	o, _ := c.CombinedOutput()
	fmt.Println(string(o))
}

// Minimum Viable To Do
// ToDo test cli
// ToDo e2e test (blackbox)
// ToDo ci to release for mac and linux
// ToDo handle session and window renaming
// ToDo create tmux plugin and hooks

// unsorted mess of To Do
//TODO write bash script wrapper for TMUX to call
//TODO docs
//TODO write gitlab-ci file - build, test, release for MacOS and Linux
//TODO test moving window to a session as it's own pane
//TODO make build scripts for Linux Windows, and darwin for AMD64 on all and ARM64 for Linux  and  ARM for darwin
//TODO make tests check for tmux availability before running
