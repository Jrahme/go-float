package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/jrahme/go-float/tmux"
)

var sinkCmd = &cobra.Command{
	Use:   "sink",
	Short: "sink a floating pane to the current window",
	Long:  "sink a floating pane to the current window",
	Run: func(cmd *cobra.Command, args []string) {
		t, err := tmux.NewLocalTmux()
		if err != nil {
			panic(err)
		}
		t.AttachPane(sinkTarget)
	},
}

var sinkTarget string

func init() {
	sinkCmd.PersistentFlags().StringVar(&sinkTarget, "sink", "", "The target floating pane to sink")
}
