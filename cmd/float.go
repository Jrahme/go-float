package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/jrahme/go-float/tmux"
)

var floatTarget string
var floatCmd = &cobra.Command{
	Use:   "float",
	Short: "float active pane",
	Long:  "Float active pane to the floating session as a window",
	Run: func(cmd *cobra.Command, args []string) {
		t, err := tmux.NewLocalTmux()
		if err != nil {
			panic(err)
		}
		t.FloatPane(floatTarget)
	},
}
