package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

var rootCmd = &cobra.Command{
	Use:   "tmux-float",
	Short: "Implementation of logic for the tmux-float plugin",
}
var socket string

func init() {
	floatCmd.PersistentFlags().StringVar(&floatTarget, "float", "", "The target float type to use")
	sinkCmd.PersistentFlags().StringVar(&sinkTarget, "sink", "", "The target floating pane to sink")
	rootCmd.PersistentFlags().StringVar(&socket, "socket", "/tmp/tmux-1000/default", "The socket of the tmux server")
	rootCmd.AddCommand(floatCmd)
	rootCmd.AddCommand(sinkCmd)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
