module gitlab.com/jrahme/go-float

go 1.13

require (
	github.com/google/uuid v1.1.3
	github.com/spf13/cobra v1.1.1
	gotest.tools/v3 v3.0.3
)
